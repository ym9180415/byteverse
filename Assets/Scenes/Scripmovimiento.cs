using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scripmovimiento : MonoBehaviour
{

  [SerializeField] private float vel;
  [SerializeField] private Rigidbody2D rb;
  [SerializeField] private Animator anim; 

  private Vector2 mov;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical"); 
        mov = new Vector2(x,y).normalized;

        anim.SetFloat("Horizontal", y);
        anim.SetFloat("Vertical", x);
    }
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + mov * vel * Time.deltaTime);
    }
}
